package BotService;

import BotService.TelegramServiceImpl;
import interfaceOfBot.BotState;
import interfaceOfBot.Constant;
import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

public class CurrencyBot extends TelegramLongPollingBot {

    static String botToken = "5015669250:AAG3mc0B6wGn-Cvl9H6avwOqS_XJL0zFDFw";
    static String botName = "Valyuta_AyriboshlashBot";
    public TelegramServiceImpl telegramServiceImpl = new TelegramServiceImpl();
    String state = null;//level

    @Override
    public String getBotUsername() {
        return botName;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            String text = update.getMessage().getText();
            if (update.getMessage().getText().equals("/start")) {
                execute(telegramServiceImpl.welcome(update));
                state = BotState.START;
            } else {
                switch (state) {
                    case BotState.START:
                        switch (text) {
                            case Constant.INFO_TEXT:
                                execute(telegramServiceImpl.currencyInfo(update));
                                state = BotState.INFO;
                                break;
                            case Constant.CONVERTOR_TEXT:
                                execute(telegramServiceImpl.currencyConvertor(update));
                                state = BotState.CONVERTOR;
                                break;
                        }
                        break;
                    case BotState.INFO:
                        switch (text) {
                            case Constant.USD:
                            case Constant.EUR:
                            case Constant.RUB:
                                execute(telegramServiceImpl.getInfo(update));
                                break;
                            case Constant.BACK:
                                execute(telegramServiceImpl.backToMenu(update));
                                state = BotState.START;
                                break;
                        }
                        break;
                    case BotState.CONVERTOR:
                        switch (text) {
                            case Constant.TO_SUM:
                                execute(telegramServiceImpl.toSum(update));
                                state = BotState.CONVERTOR_IN;
                                break;
                            case Constant.FROM_SUM:
                                execute(telegramServiceImpl.fromSum(update));
                                state = BotState.CONVERTOR_FROM;
                                break;
                        }
                        break;
                    case BotState.CONVERTOR_IN:
                        switch (text) {
                            case Constant.USD:
                            case Constant.EUR:
                            case Constant.RUB:
                                execute(telegramServiceImpl.toSumConvert(update));
                                break;
                            case Constant.BACK:
                                execute(telegramServiceImpl.backToCurrencyConvert(update));
                                state = BotState.CONVERTOR;
                                break;
                            default:
                                execute(telegramServiceImpl.toSumConvertInfo(update));
                        }
                        break;
                    case BotState.CONVERTOR_FROM:
                        switch (text) {
                            case Constant.USD:
                            case Constant.EUR:
                            case Constant.RUB:
                                execute(telegramServiceImpl.fromSumConvert(update));
                                break;
                            case Constant.BACK:
                                execute(telegramServiceImpl.backToCurrencyConvert(update));
                                state = BotState.CONVERTOR;
                                break;
                            default:
                                execute(telegramServiceImpl.fromSumConvertInfo(update));
                                break;
                        }
                        break;
                }
            }
        }

    }
}
