package BotService;

import interfaceOfBot.Constant;
import interfaceOfBot.TelegramService;
import modul.Currency;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TelegramServiceImpl implements TelegramService {
    public Currency currentCurrency = null;

    @Override
    public SendMessage welcome(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        sendMessage.setText(Constant.WELCOME_TEXT);
        sendMessage.setParseMode(ParseMode.MARKDOWN);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton(Constant.INFO_TEXT);
        KeyboardButton button2 = new KeyboardButton(Constant.CONVERTOR_TEXT);

        row.add(button1);
        row.add(button2);

        rowList.add(row);
        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        return sendMessage;
    }

    @Override
    public SendMessage currencyInfo(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        sendMessage.setText(Constant.INFO_CURRENCY_INFO);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();

        KeyboardButton usd = new KeyboardButton(Constant.USD);
        KeyboardButton rub = new KeyboardButton(Constant.RUB);
        KeyboardButton eur = new KeyboardButton(Constant.EUR);
        KeyboardButton back = new KeyboardButton(Constant.BACK);

        row1.add(usd);
        row1.add(rub);

        row2.add(eur);
        row2.add(back);

        rowList.add(row1);
        rowList.add(row2);

        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        return sendMessage;
    }

    @Override
    public SendMessage getInfo(Update update) throws IOException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        String input = update.getMessage().getText();

        Currency oneInfo = CurrencyUtil.getOneOfInfo(input);
        System.out.println(oneInfo);

        String stringBuilder = "Currency name: " +
                oneInfo.getCcyNm_EN() +
                "\n" +
                "Exchange rate: " +
                oneInfo.getRate() +
                " Sum \n" +
                "Date:\uD83D\uDCC6 " +
                oneInfo.getDate();
        sendMessage.setText(stringBuilder);
        return sendMessage;
    }

    @Override
    public SendMessage backToMenu(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        sendMessage.setText(Constant.BACK);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton(Constant.INFO_TEXT);
        KeyboardButton button2 = new KeyboardButton(Constant.CONVERTOR_TEXT);

        row1.add(button1);
        row1.add(button2);

        rowList.add(row1);

        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        return sendMessage;
    }

    @Override
    public SendMessage currencyConvertor(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton(Constant.TO_SUM);
        KeyboardButton button2 = new KeyboardButton(Constant.FROM_SUM);

        row1.add(button1);
        row1.add(button2);

        rowList.add(row1);

        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        sendMessage.setText(Constant.INFO_CURRENCY_INFO);

        return sendMessage;
    }

    @Override
    public SendMessage toSum(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        sendMessage.setText(Constant.INFO_CURRENCY_INFO);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton(Constant.USD);
        KeyboardButton button2 = new KeyboardButton(Constant.EUR);
        KeyboardButton button3 = new KeyboardButton(Constant.RUB);
        KeyboardButton button4 = new KeyboardButton(Constant.BACK);

        row1.add(button1);
        row1.add(button2);

        row2.add(button3);
        row2.add(button4);

        rowList.add(row1);
        rowList.add(row2);

        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        return sendMessage;
    }

    @Override
    public SendMessage toSumConvert(Update update) throws IOException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        String input = update.getMessage().getText();

        Currency oneInfo = CurrencyUtil.getOneOfInfo(input);
        System.out.println(oneInfo);
        currentCurrency = oneInfo;

        sendMessage.setText(Constant.VALUE);

        return sendMessage;
    }

    @Override
    public SendMessage toSumConvertInfo(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        String input = update.getMessage().getText();

        double sum = Double.parseDouble(input) * Double.parseDouble(currentCurrency.getRate());

        Locale locale = new Locale("en", "UK");
        String pattern = "###.###";

        DecimalFormat format = (DecimalFormat)
                NumberFormat.getNumberInstance(locale);
        format.applyPattern(pattern);

        String strFormat = format.format(sum);

        sendMessage.setText(strFormat + " sum");

        return sendMessage;
    }

    @Override
    public SendMessage fromSum(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        sendMessage.setText(Constant.INFO_CURRENCY_INFO);

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);

        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardRow row2 = new KeyboardRow();

        KeyboardButton button1 = new KeyboardButton(Constant.USD);
        KeyboardButton button2 = new KeyboardButton(Constant.EUR);
        KeyboardButton button3 = new KeyboardButton(Constant.RUB);
        KeyboardButton button4 = new KeyboardButton(Constant.BACK);

        row1.add(button1);
        row1.add(button2);

        row2.add(button3);
        row2.add(button4);

        rowList.add(row1);
        rowList.add(row2);

        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        return sendMessage;
    }

    @Override
    public SendMessage fromSumConvert(Update update) throws IOException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));
        String text = update.getMessage().getText();

        Currency oneInfo = CurrencyUtil.getOneOfInfo(text);
        System.out.println(oneInfo);
        currentCurrency = oneInfo;

        sendMessage.setText(Constant.VALUE);
        return sendMessage;
    }

    @Override
    public SendMessage fromSumConvertInfo(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        String value = update.getMessage().getText();
        double v = Double.parseDouble(value);
        double rate = Double.parseDouble(currentCurrency.getRate());


        sendMessage.setText(v / rate + currentCurrency.getCcyNm_UZ());
        return sendMessage;
    }

    @Override
    public SendMessage backToCurrencyConvert(Update update) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(update.getMessage().getChatId()));

        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setOneTimeKeyboard(true);
        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row = new KeyboardRow();
        KeyboardButton toSum = new KeyboardButton(Constant.TO_SUM);
        KeyboardButton fromSum = new KeyboardButton(Constant.FROM_SUM);
        row.add(toSum);
        row.add(fromSum);
        rowList.add(row);
        replyKeyboardMarkup.setKeyboard(rowList);

        sendMessage.setReplyMarkup(replyKeyboardMarkup);

        sendMessage.setText(Constant.BACK);
        return sendMessage;
    }
}
