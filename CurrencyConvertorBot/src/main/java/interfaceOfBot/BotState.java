package interfaceOfBot;

public interface BotState {
    String START = "START";
    String INFO = "INFO";
    String CONVERTOR = "CONVERTOR";
    String CONVERTOR_IN = "CONVERTOR_IN";
    String CONVERTOR_FROM = "CONVERTOR_FROM";
}
