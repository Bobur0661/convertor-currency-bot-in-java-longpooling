package interfaceOfBot;

public interface Constant {

    String WELCOME_TEXT = "Welcome to Currency\uD83D\uDCB8bot";
    String INFO_TEXT = "Currency info\uD83D\uDCCA";
    String CONVERTOR_TEXT = "Currency convertor♻️";

    String INFO_CURRENCY_INFO = "Select one";
    String USD = "USD";
    String EUR = "EUR";
    String RUB = "RUB";

    String BACK = "BACK↩️";

    String TO_SUM = "Currency  ➡  So'm";
    String FROM_SUM = "So'm  ➡️ Currency";
    String VALUE = "Enter the value";
}
