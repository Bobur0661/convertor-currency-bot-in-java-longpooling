package interfaceOfBot;

import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.IOException;

public interface TelegramService {
    SendMessage welcome(Update update);

    SendMessage currencyInfo(Update update);

    SendMessage getInfo(Update update)  throws IOException;

    SendMessage backToMenu(Update update);

    SendMessage currencyConvertor(Update update);

    SendMessage toSum(Update update);

    SendMessage toSumConvert(Update update)  throws IOException;

    SendMessage toSumConvertInfo(Update update);

    SendMessage fromSum(Update update);

    SendMessage fromSumConvert(Update update)  throws IOException;

    SendMessage fromSumConvertInfo(Update update) throws IOException;

    SendMessage backToCurrencyConvert(Update update);

}
